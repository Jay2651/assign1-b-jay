﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assi1b-jay.aspx.cs" Inherits="Ass1b_jay.Assi1b_jay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Humber College Admission Inquiry</h1>
        <div>
            <div runat="server"  id="Student_info">

            </div>
            <asp:Label runat="server" ID="name1">First Name</asp:Label>
            <asp:TextBox runat="server" ID="FirstName" placeholder="Enter Your First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="fN" ControlToValidate="FirstName" ErrorMessage="enter your First Name." >
            </asp:RequiredFieldValidator>
            <br />
	    
            <asp:Label runat="server" ID="name2">Last Name</asp:Label>
            <asp:TextBox runat="server" ID="LastName" placeholder="Enter Your Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="LN" ControlToValidate="LastName" ErrorMessage="enter your Last Name" >
            </asp:RequiredFieldValidator>
            <br />

	    <asp:Label runat="server" ID="ph1">Phone No</asp:Label>
            <asp:TextBox runat="server" ID="PhoneNo" placeholder="Enter Your Number"></asp:TextBox>
            <asp:RegularExpressionValidator  runat="server" ID="p1" ControlToValidate="PhoneNo" ErrorMessage="Please enter valid Mobile number."
            ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
            <br />

	    <asp:Label runat="server" ID="E1">E-mail</asp:Label>
            <asp:TextBox runat="server" ID="EMail" placeholder="Enter Your E-mail"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="mail" ControlToValidate="EMail" ErrorMessage="Please enter valid email." 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            
            <asp:Label runat="server" ID="inq">Inquiry</asp:Label>
            <asp:DropDownList runat="server" ID="inq1">
            <asp:ListItem runat="server">Admission</asp:ListItem>
            <asp:ListItem runat="server">Program Selection</asp:ListItem>
            <asp:ListItem runat="server">Application Process</asp:ListItem>
            <asp:ListItem runat="server">Program Information</asp:ListItem>
            <asp:ListItem runat="server">Faculty List</asp:ListItem>
	    <asp:ListItem runat="server">Campus Facility</asp:ListItem>
            <asp:ListItem runat="server">College Contact Information</asp:ListItem>
            </asp:DropDownList>
            <br />
            
            <asp:Label runat="server" ID="Prev">Previous Qualification</asp:Label>
            <div runat="server" id="Qualification_Dropdown">
            <asp:DropDownList runat="server" ID="Prev1">
            <asp:ListItem runat="server">After 10th</asp:ListItem>
            <asp:ListItem runat="server">After High School</asp:ListItem>
            <asp:ListItem runat="server">After Diploma</asp:ListItem>
            </asp:DropDownList>
             </div>
            <br />

	    <asp:label runat="server">Intrested in feild</asp:label>
	    <asp:RadioButton runat="server" ID="IF1" GroupName="Intrested" text="Computer Hardware"/>
            <asp:RadioButton runat="server" ID="RB2" GroupName="Intrested" text="Web Design & Development"/>
            <asp:RadioButton runat="server" ID="RB3" GroupName="Intrested" text="Information & Security"/>
            <br />

	    <asp:Label runat="server" ID="ser">Subject Intrested</asp:Label>
            <div runat="server" id="Subject_dropdown">
            <asp:CheckBox runat="server" ID="Ec" Text="E-commerce Website Development"/>
            <asp:CheckBox runat="server" ID="WS" Text="Web Development & Security Assurance"/>
	    <asp:CheckBox runat="server" ID="J" Text="Java +"/>
	    <asp:CheckBox runat="server" ID="AD" Text="Android Application Development"/>
                </div>
           <br />
	   
                <asp:Label runat="server">Inquiry Fees Payment</asp:Label>
                <asp:RadioButtonList runat="server" ID="InqrPayment">
                    <asp:ListItem>Pay by bank transfer</asp:ListItem>
                    <asp:ListItem>Pay by Credit/Debit Card</asp:ListItem>

                </asp:RadioButtonList>
            <br />

            <asp:Button runat="server" Text="Submit" ID="Submit" OnClick="submitrecord" />
          
	    <asp:ValidationSummary id="validSum" HeaderText="You must enter a value in the following fields:" runat="server"/>

            <div runat="server" id="StudentResc"></div>
     
            <footer runat="server" id="rescfooter">

            </footer>
       
     </div>
    </form>
    </body>
</html>

 