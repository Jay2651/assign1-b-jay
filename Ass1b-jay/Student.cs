﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ass1b_jay
{
    public class Student
    {
        private string StudName;
        private string StudEmail;
        private string StudPhoneno;

        public Student()
        {

        }
        public string studName
        {
            get { return StudName; }
            set { StudName = value; }
        }
        public string studEmail
        {
            get { return StudEmail; }
            set { StudEmail = value; }
        }
        public string studPhoneNo

        {
            get { return StudPhoneno; }
            set { StudPhoneno = value; }
        }
    }

}