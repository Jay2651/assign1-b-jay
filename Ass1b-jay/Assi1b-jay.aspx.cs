﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ass1b_jay
{
    public partial class Assi1b_jay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           


        }

        protected void submitrecord(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            Student_info.InnerHtml = "Thank you";
            
         //  student variables
            string StdName = FirstName.Text.ToString();
            string StdEmail = EMail.Text.ToString();
            string StdPhoneno = PhoneNo.Text.ToString();

            Student newStudent1 = new Student();

            newStudent1. studName= StdName;
            newStudent1.studEmail = StdEmail;
            newStudent1.studPhoneNo = StdPhoneno;

            //inq variables
            string qualification = Prev1.SelectedItem.Value.ToString();
            Inquiry newinq = new Inquiry(qualification);
            //subjects variables

            List<string> subject = new List<string>();
            foreach(Control control in Qualification_Dropdown.Controls)
            {
                if(control.GetType()==typeof(CheckBox))
                {
                    CheckBox sub = (CheckBox)control;
                    if(sub.Checked)
                    {
                        subject.Add(sub.Text);
                    }
                }
            }
            subject newsubject = new subject(subject);

            final newfinal = new final(newStudent1, newinq, newsubject);
            StudentResc.InnerHtml = newfinal.PrintReciept();


        }


    }
}